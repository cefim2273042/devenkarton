<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Messages;
use App\Form\MessageType;
use App\Repository\CategoryRepository;
use App\Repository\MessagesRepository;
use App\Repository\VideoRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_DEV')]
class HomeController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(Request $request, FileUploader $fileUploader, MessagesRepository $messagesRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $messages = new Messages();
        $messagesForm = $this->createForm(MessageType::class, $messages);
        $messagesForm->handleRequest($request);

        if ($messagesForm->isSubmitted() && $messagesForm->isValid()) {
            $imageFile = $messagesForm->get('image')->getData();

            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $messages->setImage($imageFileName);
            }

            $messages->setAuteur($user);
            $messagesRepository->save($messages, true);
            $this->addFlash('success', 'Votre message a bien été enregistré !');
            return $this->redirectToRoute('index');
        }

        return $this->render('index.html.twig', [
            'messages' => $messagesRepository->findBy([], ['postedAt' => 'DESC']),
            'form' => $messagesForm->createView(),
        ]);
    }

    #[Route('/video', name: 'video')]
    public function video(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();
        $categoriesArray = [];

        foreach ($categories as $category) {
            if ($category->getVideos()->count() > 0) {
                $categoriesArray[] = $category;
            }
        }

        return $this->render('video/index.html.twig', [
            'categories' => $categoriesArray,
        ]);
    }

    #[Route('/video/categorie/{id}', name: 'video_show')]
    public function videoShow(VideoRepository $videoRepository, Category $category): Response
    {
        $videos = $videoRepository->findBy(['categorie' => $category]);

        return $this->render('video/show.html.twig', [
            'categorie' => $category,
            'videos' => $videos,
        ]);
    }
}
