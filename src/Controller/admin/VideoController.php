<?php

namespace App\Controller\admin;

use App\Entity\Video;
use App\Form\admin\VideoType;
use App\Repository\VideoRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/video', name: 'admin_video_')]
class VideoController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(VideoRepository $videoRepository): Response
    {
        return $this->render('admin/video/index.html.twig', [
            'videos' => $videoRepository->findAll(),
        ]);
    }

    #[Route('/create', name: 'create', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'edit', requirements: ['id' => '\d'], methods: ['GET', 'POST'])]
    public function manage(Request $request, VideoRepository $videoRepository, ?int $id, FileUploader $fileUploader): Response
    {
        $videoEntity = new Video();

        if (null !== $id) {
            $videoEntity = $videoRepository->findOneById($id);
        }

        $form = $this->createForm(VideoType::class, $videoEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $video = $form->get('url')->getData();

            if ($video) {
                $videoFileName = $fileUploader->upload($video);
                $videoEntity->setUrl($videoFileName);
            }
            $videoRepository->save($videoEntity, true);

            $this->addFlash('success', 'La vidéo a bien été ajoutée.');
            return $this->redirectToRoute('admin_video_index', [], Response::HTTP_SEE_OTHER);
        }


        return $this->render('admin/video/create.html.twig', [
            'form' => $form,
        ]);
    }
}
