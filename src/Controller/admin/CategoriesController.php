<?php

namespace App\Controller\admin;

use App\Entity\Category;
use App\Form\admin\CategoryType;
use App\Repository\CategoryRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/admin/categories', name: 'admin_categories_')]
#[IsGranted('ROLE_ADMIN')]
class CategoriesController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(CategoryRepository $categoryRepository, Request $request, FileUploader $fileUploader): Response
    {
        $cat = new Category();
        $categoryForm = $this->createForm(CategoryType::class, $cat);
        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $imageFile = $categoryForm->get('image')->getData();

            if($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $cat->setIllustration($imageFileName);
            }

            $categoryRepository->save($cat, true);

            $this->addFlash('success', 'La catégorie a bien été ajoutée');
            return $this->redirectToRoute('admin_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/categories.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'form' => $categoryForm->createView(),
        ]);
    }

    #[Route('/{id}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(CategoryRepository $categoryRepository, Request $request, Category $category, FileUploader $fileUploader): Response
    {
        $categoryForm = $this->createForm(CategoryType::class, $category);
        $categoryForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $imageFile = $categoryForm->get('image')->getData();

            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $category->setIllustration($imageFileName);
            }

            $categoryRepository->save($category, true);

            $this->addFlash('success', 'La catégorie a bien été modifiée');
            return $this->redirectToRoute('admin_categories_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/editCat.html.twig', [
            'category' => $category,
            'form' => $categoryForm->createView(),
        ]);
    }
}
