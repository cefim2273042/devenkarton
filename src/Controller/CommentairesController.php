<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Entity\Messages;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentairesController extends AbstractController
{
    #[Route('/{messageId}/new', name: 'commentaires_new')]
    public function index(
        Request                $request,
        Messages               $messageId,
        FileUploader           $fileUploader,
        CommentairesRepository $commentairesRepository
    ): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $commentaires = new Commentaires();
        $commentairesForm = $this->createForm(CommentairesType::class, $commentaires);
        $commentairesForm->handleRequest($request);

        if ($commentairesForm->isSubmitted() && $commentairesForm->isValid()) {
            $imageFile = $commentairesForm->get('image')->getData();

            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $commentaires->setImage($imageFileName);
            }

            $commentaires->setAuteur($user)->setTweet($messageId);
            $commentairesRepository->save($commentaires, true);

            $this->addFlash('success', 'Votre message a bien été enregistré !');
            return $this->redirectToRoute('index');
        }

        return $this->render('commentaires/new.html.twig', [
            'form' => $commentairesForm->createView(),
            'message' => $messageId,
        ]);
    }
}
