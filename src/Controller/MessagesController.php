<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Entity\Messages;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use App\Repository\MessagesRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessagesController extends AbstractController
{
    #[Route('/{id}/show', name: 'messages_show', methods: ['GET', 'POST'])]
    public function show(Messages $message, Request $request,
                         FileUploader $fileUploader,
                         CommentairesRepository $commentairesRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $commentaire = new Commentaires();
        $commentaireForm = $this->createForm(CommentairesType::class, $commentaire);
        $commentaireForm->handleRequest($request);

        if ($commentaireForm->isSubmitted() && $commentaireForm->isValid())
        {
            $imageFile = $commentaireForm->get('image')->getData();

            if ($imageFile)
            {
                $imageFileName = $fileUploader->upload($imageFile);
                $commentaire->setImage($imageFileName);
            }

            $commentaire->setAuteur($user);
            $commentaire->setTweet($message);
            $commentairesRepository->save($commentaire, true);

            $this->addFlash('success', 'Votre commentaire a bien été enregistré !');
            return $this->redirectToRoute('messages_show', ['id' => $message->getId()]);
        }

        return $this->render('messages/show.html.twig', [
            'message' => $message,
            'form' => $commentaireForm->createView(),
            'user' => $user,
        ]);
    }

    #[Route('/{id}/delete', name: 'message_delete', methods: ['GET', 'POST'])]
    public function delete(Request $request, Messages $messages,
                           CommentairesRepository $commentairesRepository,
    MessagesRepository $messagesRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        if ($this->isCsrfTokenValid('delete'.$messages->getId(), $request->request->get('_token')))
        {
            if ($user === $messages->getAuteur() || $this->isGranted('ROLE_ADMIN'))
            {
                $commentaires = $commentairesRepository->findBy(['Tweet' => $messages->getId()]);

                foreach ($commentaires as $commentaire)
                {
                    $commentairesRepository->remove($commentaire, true);
                }

                $messagesRepository->remove($messages, true);
                $this->addFlash('success', 'Votre tweet a bien été supprimé !');
            }
            else
            {
                $this->addFlash('danger', 'Vous ne pouvez pas supprimer le tweet d\'un autre utilisateur !');
            }
        }

        return $this->redirectToRoute('index');
    }
}
