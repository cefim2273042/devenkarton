<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/profil/{id}', name: 'user_index')]
    #[Security('is_granted("ROLE_USER") and user == id')]
    public function index(User $id, Request $request, UserRepository $userRepository, FileUploader $fileUploader): Response
    {
        $userForm = $this->createForm(UserEditType::class, $id);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $avatar = $userForm->get('avatar')->getData();
            if ($avatar) {
                $avatarFileName = $fileUploader->upload($avatar);
                $id->setAvatar($avatarFileName);
            }
            $userRepository->save($id, true);

            $this->addFlash('success', 'Votre profil a bien été modifié');
            return $this->redirectToRoute('user_index', ['id' => $id->getId()]);
        }

        return $this->render('user/index.html.twig', [
            'form' => $userForm->createView(),
            'user' => $id,
        ]);
    }
}
