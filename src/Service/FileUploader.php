<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    public function __construct(
        private $targetDirectoryImage,
        private $targetDirectoryVideo,
        private SluggerInterface $slugger,
    ) {
    }

    public function upload(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename. '-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory($file->guessExtension()), $fileName);
        } catch (FileException $exception)
        {
            throw new FileException("Erreur lors de l'upload de l'image : " . $exception->getMessage());
        }

        return $fileName;
    }

    public function getTargetDirectory(string $extension): string
    {
        if ($extension === 'mp4') {
            return $this->targetDirectoryVideo;
        } else {
           return $this->targetDirectoryImage;
        }
    }
}
