<?php

namespace App\EventListener;

use App\Entity\Category;
use App\Entity\Commentaires;
use App\Entity\Messages;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class HashtagExtractListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Messages && !$entity instanceof Commentaires) {
            return;
        }

        $entityManager = $args->getObjectManager();
        $hashtags = [];

        $message = $entity->getMessage();
        preg_match_all('/#(\w+)/', $message, $matches);

        if (!empty($matches[1])) {
            $hashtags = $matches[1];
        }

        foreach ($hashtags as $hashtag) {
            $category = $entityManager->getRepository(Category::class)->findOneBy(['name' => $hashtag]);

            if (!$category) {
                $category = new Category();
                $category->setName($hashtag);
                $entity->addCategory($category);
            }

            $category->incrementCounter();
            $entity->addCategory($category);

            $entityManager->persist($category);
            $entityManager->persist($entity);
        }
        $entityManager->flush();
    }
}
