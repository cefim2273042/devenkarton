# Vie de stage, les dev'en Karton !

## Présentation

Petit site web afin de rester en contact lors du stage de la meilleure promo du CEFIM : Les DWWM2023-1

### Le projet

Site créé sur Symfony permettant l'inscription, la connexion et la publication de petit message pour raconter sa journée
de dev ou une anecdote.

## Installation

### Prérequis

- Symfony 6.2.x
- PHP 8.x
- Composer 2.x
- NodeJS 16.x
- MySQL 8.x
- Symfony CLI

### Installation

Récupération du projet avec un git clone :  
`git clone git@gitlab.cefim-formation.org:Alexane/vdm.git`

Installation des dépendances :  
`composer install`

Création de la base de données :  
Créer un fichier .env.local et y ajouter les informations de connexion à la base de données, puis  
`symfony console doctrine:database:create`

Mise en place des migrations :  
`symfony console doctrine:migrations:migrate`

Installation des dépendances JS (pour utiliser Sass avec Encore) :  
`npm install`

Compilation des assets :   
`npm run build`

Si vous voulez faire des modifications sur les assets, vous pouvez lancer la commande suivante pour que les assets
soient recompilés à chaque modification :  
`npm run watch`
Les modifications se font bien évidemment dans le dossier assets, elles sont ensuite compilées dans le dossier public
grâce à npm.

### Lancement
Lancer le serveur Symfony :  
`symfony serve`